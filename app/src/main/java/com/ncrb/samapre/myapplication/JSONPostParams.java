package com.ncrb.samapre.myapplication;

/**
 * Created by Lenovo on 16-02-2018.
 */

import android.support.annotation.Keep;

import java.util.Dictionary;
import java.util.List;
import java.util.Map;


/**
 * Class to represent a category in app.
 * @author
 */
public class JSONPostParams {

    @Keep
    String username;
    String password;

    @Keep
    String coco;

    String hello;

    String MATCH_CRITERIA_UAP;

    String MATCH_CRITERIA_SEIZED;

    String MATCH_CRITERIA_REGISTERED;

    Integer MV_TYPECD;
    Integer MV_MAKECD;
    Integer MV_MODELCD;
    Integer District_Cd;
    String REGISTRATION_NO;
    String CHASSIS_NO;
    String ENGINE_NO;
    Integer VType_Cd;
    String relation;
    String religion;
    String MVColor;
    Integer MVCOLOR_CD;
    Integer MVMake;
    Integer STATE_CD,DISTRICT_CD,PS_CD;

    String MATCH_CRITERIA_ARR,MATCH_CRITERIA_CON,MATCH_CRITERIA_PO,MATCH_CRITERIA_CS,MATCH_CRITERIA_HO;
    String P_FName,P_MName,P_LName, P_Alias, P_RelativeName,lang_cd ;
    Integer P_GCODE, P_RELGNCODE,P_RELTYPECODE, P_AgeFrom, P_AgeTo,P_STATE_CD,P_DISTRICT_CD,P_PS_CD;
    double P_HEIGHTFROM, P_HEIGHTTO;

    String FxdState_Cd;

    // edit vj
    String m_service;
    String mperson_reg_num;
    String seed;




    ///Map Message = new HashMap();	Message.put("title", "Test message");Message.get("title")
    public JSONPostParams(String method_name, Map mapParams) {


        // @logic may in future we need to add more parameters
        if (method_name == "mLoginVerify") {

            this.username = mapParams.get("username").toString();
            this.password = mapParams.get("password").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mVehicleTypeConnect") {

            this.hello = mapParams.get("hello").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mPropertySearch") {

            this.MATCH_CRITERIA_UAP = mapParams.get("MATCH_CRITERIA_UAP").toString();
            this.MATCH_CRITERIA_SEIZED = mapParams.get("MATCH_CRITERIA_SEIZED").toString();
            this.MATCH_CRITERIA_REGISTERED = mapParams.get("MATCH_CRITERIA_REGISTERED").toString();
            this.MV_TYPECD = (int)mapParams.get("MV_TYPECD");
            this.MV_MAKECD = (int)mapParams.get("MV_MAKECD");
            this.MV_MODELCD = (int)mapParams.get("MV_MODELCD");
            this.REGISTRATION_NO = mapParams.get("REGISTRATION_NO").toString();
            this.CHASSIS_NO = mapParams.get("CHASSIS_NO").toString();
            this.ENGINE_NO = mapParams.get("ENGINE_NO").toString();
            this.MVCOLOR_CD=(int)mapParams.get("MVCOLOR_CD");
            this.STATE_CD = (int)mapParams.get("STATE_CD");
            this.DISTRICT_CD = (int)mapParams.get("DISTRICT_CD");
            this.PS_CD = (int)mapParams.get("PS_CD");
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mManufacturerConnect") {

            this.VType_Cd = (int)mapParams.get("VType_Cd");
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mRelationTypeConnect") {

            this.relation = mapParams.get("relation").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }

        else if (method_name == "mReligionConnect") {

            this.religion = mapParams.get("religion").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mMVColorConnect") {

            this.MVColor = mapParams.get("MVColor").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mMVModelConnect") {

            this.MVMake = (int)mapParams.get("MVMake");
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }

        else if (method_name == "mDistrictConnect") {

            this.FxdState_Cd = mapParams.get("FxdState_Cd").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }
        else if (method_name == "mPoliceStationConnect") {

            this.District_Cd = (int)mapParams.get("District_Cd");
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();

        }

        else if (method_name == "mPersonSearch") {

            this.MATCH_CRITERIA_ARR = mapParams.get("MATCH_CRITERIA_ARR").toString();
            this.MATCH_CRITERIA_CON = mapParams.get("MATCH_CRITERIA_CON").toString();
            this.MATCH_CRITERIA_PO = mapParams.get("MATCH_CRITERIA_PO").toString();
            this.MATCH_CRITERIA_CS = mapParams.get("MATCH_CRITERIA_CS").toString();
            this.MATCH_CRITERIA_HO = mapParams.get("MATCH_CRITERIA_HO").toString();
            this.P_FName = mapParams.get("P_FName").toString();
            this.P_MName = mapParams.get("P_MName").toString();
            this.P_LName = mapParams.get("P_LName").toString();
            this.P_Alias = mapParams.get("P_Alias").toString();
            this.P_GCODE = (int)mapParams.get("P_GCODE");
            this.P_RELGNCODE = (int)mapParams.get("P_RELGNCODE");
            this.P_RELTYPECODE = (int)mapParams.get("P_RELTYPECODE");
            this.P_RelativeName = mapParams.get("P_RelativeName").toString();
            this.P_AgeFrom = (int)mapParams.get("P_AgeFrom");
            this.P_AgeTo = (int)mapParams.get("P_AgeTo");
            this.P_HEIGHTFROM = (double)mapParams.get("P_HEIGHTFROM");
            this.P_HEIGHTTO = (double)mapParams.get("P_HEIGHTTO");
            this.P_STATE_CD = (int)mapParams.get("STATE_CD");
            this.P_DISTRICT_CD = (int)mapParams.get("DISTRICT_CD");
            this.P_PS_CD = (int)mapParams.get("PS_CD");
            this.lang_cd = mapParams.get("lang_cd").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();
        }

        else if (method_name == "mGetImagesBlob") {

            this.mperson_reg_num = mapParams.get("mperson_reg_num").toString();
            this.m_service = mapParams.get("m_service").toString();
            this.seed = mapParams.get("seed").toString();



        }

    }// end json post params




}// end json post params