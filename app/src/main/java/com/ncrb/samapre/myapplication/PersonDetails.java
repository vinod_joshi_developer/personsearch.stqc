package com.ncrb.samapre.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PersonDetails extends AppCompatActivity {
    ImageButton btn_back;

    AppPreferences objAppPreferences;
    Singleton singleton;
    ImageView accusedimage;

    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);



        singleton = Singleton.getInstance();

        // core objects
        objAppPreferences = new AppPreferences(PersonDetails.this);


        // update count value
        updateUserCount(PersonDetails.this, Constants.SearchTypePerson);


        TextView textView_person_full_name,textView_person_alias, textView_person_relation_type;
        TextView textView_person_gender, textView_person_age, textView_person_religion, textView_person_nationality;
        TextView textView_person_height_from, textView_person_height_to, textView_person_uid;
        TextView textView_person_State, textView_person_district, textView_person_ps;
        TextView textView_person_FIR_reg_num, textView_person_full_FIR_reg_num, textView_person_FIR_date;
        TextView textView_person_type,textView_person_relative_name,textView_person_permanent,textView_person_present;
        Button btn_pdf;



        btn_back=(ImageButton)findViewById(R.id.btn_Back);
        btn_pdf=(Button)findViewById(R.id.btn_pdf);

        textView_person_full_name=(TextView)findViewById(R.id.txt_person_detail_Full_Name);
        textView_person_alias=(TextView)findViewById(R.id.txt_person_detail_alias);
        textView_person_relation_type=(TextView)findViewById(R.id.txt_person_detail_Relation_Type);
        textView_person_relative_name=(TextView)findViewById(R.id.txt_person_detail_Relative_Name);
        textView_person_gender=(TextView)findViewById(R.id.txt_person_detail_Gender);
        textView_person_age=(TextView)findViewById(R.id.txt_person_detail_age);
        textView_person_religion=(TextView)findViewById(R.id.txt_person_detail_religion);
        textView_person_nationality=(TextView)findViewById(R.id.txt_person_detail_nationality);
        textView_person_height_from=(TextView)findViewById(R.id.txt_person_detail_Height_From);
        textView_person_height_to=(TextView)findViewById(R.id.txt_person_detail_Height_To);
        textView_person_uid=(TextView)findViewById(R.id.txt_person_detail_UID);
        textView_person_State=(TextView)findViewById(R.id.txt_person_detail_State);
        textView_person_district=(TextView)findViewById(R.id.txt_person_detail_district);
        textView_person_ps=(TextView)findViewById(R.id.txt_person_detail_ps);
        textView_person_FIR_reg_num=(TextView)findViewById(R.id.txt_person_detail_fir_no);
        //textView_person_full_FIR_reg_num=(TextView)findViewById(R.id.txt_person_detail_Full_FIR_NUMBER);
        textView_person_FIR_date=(TextView)findViewById(R.id.txt_person_detail_FIR_Date);
        textView_person_type=(TextView)findViewById(R.id.txt_person_detail_type);
        textView_person_permanent=(TextView)findViewById(R.id.txt_person_Permanent_Address);
        textView_person_present=(TextView)findViewById(R.id.txt_person_Present_Address);

        accusedimage=(ImageView)findViewById(R.id.accusedimage) ;

        Bundle data= getIntent().getExtras();


        textView_person_full_name.setText(data.getString("FULL_NAME"));
        textView_person_alias.setText(data.getString("ALIAS"));
        textView_person_relation_type.setText(data.getString("RELATION_TYPE"));
        textView_person_relative_name.setText(data.getString("RELATIVE_NAME"));
        textView_person_gender.setText(data.getString("GENDER"));
        textView_person_age.setText(data.getString("AGE"));
        textView_person_religion.setText(data.getString("RELIGION"));
        textView_person_nationality.setText(data.getString("NATIONALITY"));
        textView_person_height_from.setText(data.getString("HEIGHT_FROM_CM"));
        textView_person_height_to.setText(data.getString("HEIGHT_TO_CM"));
        textView_person_uid.setText(data.getString("UID_NUM"));
        textView_person_State.setText(data.getString("STATE_ENG"));
        textView_person_district.setText(data.getString("DISTRICT"));
        textView_person_ps.setText(data.getString("PS"));
        textView_person_FIR_reg_num.setText(data.getString("REGISTRATION_NUM"));
        //textView_person_full_FIR_reg_num.setText(data.getString("REGISTRATION_NUM"));
        textView_person_FIR_date.setText(data.getString("REG_DT"));
        textView_person_type.setText(data.getString("MatchingCriteria"));
        textView_person_permanent.setText(data.getString("PERMANENT"));
        textView_person_present.setText(data.getString("PRESENT"));

        String getimage=data.getString("UploadedFile");

        // find image of person
        if(Constants.API_PROGRAMMING_STACK == Constants.STACK_JAVA) {

            if(!data.getString("accusedsrNo").equals("") || !data.getString("accusedsrNo").equals(null) ){

                // java stack fetch image of person on the basis of missing person number
                try {
                    GetImagePerson(data.getString("accusedsrNo"));
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } else {

            // logic for java stack, if miss reg num not appears
            if(!data.getString("UploadedFile").equals("") || !data.getString("UploadedFile").equals(null) ){

                setImagePerson(data.getString("UploadedFile"));

            }

        }



        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPDF(view);
            }
        });

    }

    public void setImagePerson(String base64) {

        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);

        //Utils.printv("getimage="+decodedString);

        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        System.out.println("decoded byte="+ decodedByte );

        accusedimage.setImageBitmap(decodedByte);
    }


    /**
     *
     *
     *
     * */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }


    /**
     *
     *
     *
     * */


    public void printPDF(View view) {
        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);
        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this,findViewById(R.id.pdfLayout)), null);
    }


    /**
     *
     *
     *
     * */

    // create and update count dynamically
    public void updateUserCount(Context context, String searchType) {


        String date = Utils.getDateTime("dd-MM-yyyy");

        String uniqueKey = searchType+" "+date+" "+this.singleton.username;

        try {

            // core objects
            objAppPreferences = new AppPreferences(context);

            String checkUser = objAppPreferences.getUserDefaults(uniqueKey);

            if(!checkUser.equals("")) {
                int count = Integer.parseInt(checkUser);
                count++;
                objAppPreferences.setUserDefaults(uniqueKey, ""+count);
                Utils.printv("Set Preference "+uniqueKey+"count: "+count);
            } else {
                objAppPreferences.setUserDefaults(uniqueKey, "1");
                Utils.printv("Set Preference "+uniqueKey+"count: 1");
            }

        }catch (Exception e){
            e.printStackTrace();
            objAppPreferences.setUserDefaults(uniqueKey, "1");
            Utils.printv("Set Preference "+uniqueKey+"count: 1");
        }

    }// end update count


    /**
     *
     *
     *
     * */

    public void GetImagePerson(String msgNumber) throws Exception {

        mProgressDialog.setMessage("Please wait...");
        this.mProgressDialog.show();

        Map postParams = new HashMap();
        postParams.put("mperson_reg_num", msgNumber);
        postParams.put("m_service", "mGetImagesBlob");


        JSONPostParams jsonPostParams = new JSONPostParams("mGetImagesBlob", postParams);


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);


        apiCaller.mGetImagesBlob(jsonPostParams, new Callback<WSPGetBlobImageConnect>() {

            @Override
            public void failure(RetrofitError arg0) {

                Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


            }// end failure

            @Override
            public void success(WSPGetBlobImageConnect result, Response response) {

                System.out.println("RESULT status " + result.STATUS_CODE);

                if (result.STATUS_CODE.toString().equals("200")) {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();

                    setImagePerson(result.BASE64IMAGE);

                } else {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();


                    Toast.makeText(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                }
            }// end success
        });

    }// end GetImagePerson

}// end main class
