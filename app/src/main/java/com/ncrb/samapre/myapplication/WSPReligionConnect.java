package com.ncrb.samapre.myapplication;

/**
 * Created by Lenovo on 16-02-2018.
 */

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPReligionConnect {

    public String STATUS_CODE;
    public String STATUS;
    public String MESSAGE;
    public String hello;

    public List <String> MV_TYPE;
    public List <Integer> MV_TYPE_CD;
    public List <String> MV_MAKE;
    public List <Integer> MV_MAKE_CD;

    public List <String> RELATION_TYPE;
    public List <Integer> RELATION_TYPE_CD;


}// end main class

