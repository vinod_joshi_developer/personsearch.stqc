package com.ncrb.samapre.myapplication;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;


public interface ApiCaller {



    //    //@Headers("Content-Type: application/json")
    //    //@POST("/mGetCitizenLoginDetailsConnect")
    //    @Headers("Connection:close")
    //    @POST("/mLoginVerify")
    //    public void mLoginVerify(@Body JSONPostParams jsonPostParams,
    //                             retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mVehicleTypeConnect")
    //    public void mVehicleTypeConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mPropertySearch")
    //    public void mPropertySearch(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mManufacturerConnect")
    //    public void mManufacturerConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //
    //    @Headers("Connection:close")
    //    @POST("/mRelationTypeConnect")
    //    public void mRelationTypeConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mReligionConnect")
    //    public void mReligionConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPReligionConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mMVColorConnect")
    //    public void mMVColorConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mMVModelConnect")
    //    public void mMVModelConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mPersonSearch")
    //    public void mPersonSearch(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mDistrictConnect")
    //    public void mDistrictConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);
    //
    //    @Headers("Connection:close")
    //    @POST("/mPoliceStationConnect")
    //    public void mPoliceStationConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);





    /**
     *
     *  API caller for JAVA stack
     *
     *
     * **/

    //@Headers("Content-Type: application/json")
    //@POST("/mGetCitizenLoginDetailsConnect")
    @Headers("Connection:close")
    @POST("/mWebService")
    public void mLoginVerify(@Body JSONPostParams jsonPostParams,
                             retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mVehicleTypeConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mPropertySearch(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mManufacturerConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);


    @Headers("Connection:close")
    @POST("/mWebService")
    public void mRelationTypeConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mReligionConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPReligionConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mMVColorConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mMVModelConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Connection:close")
    @POST("/mWebService")
    public void mPersonSearch(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);


    @Headers("Connection:close")
    @POST("/mWebService")
    public void mGetImagesBlob(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPGetBlobImageConnect> callback);


    @Headers("Connection:close")
    @POST("/mWebService")
    public void mDistrictConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);


    @Headers("Connection:close")
    @POST("/mWebService")
    public void mPoliceStationConnect(@Body JSONPostParams jsonPostParam,retrofit.Callback<WSPLoginConnect> callback);




    // -------------------------------------------------------------------------------


    /**
     *
     *  API caller for Demo API stack
     *
     *
     * **/



                   /*
                   mLoginVerify
                   https://www.jasonbase.com/things/n5GD.json

                   mVehicleTypeConnect
                   https://www.jasonbase.com/things/qDG5.json

                   mPropertySearch
                   https://www.jasonbase.com/things/Gxwz.json


                   mManufacturerConnect
                   https://www.jasonbase.com/things/Z7G4.json

                   mRelationTypeConnect
                   https://www.jasonbase.com/things/p1GY.json


                   mReligionConnect
                   https://www.jasonbase.com/things/XbQL.json


                   mMVColorConnect
                   https://www.jasonbase.com/things/VJll.json

                   mMVModelConnect
                   https://www.jasonbase.com/things/DGL1.json

                   mPersonSearch
                   https://www.jasonbase.com/things/47X1.json


                   mDistrictConnect
                   https://www.jasonbase.com/things/Y2aL.json


                   mPoliceStationConnect
                   https://www.jasonbase.com/things/WKd2.json






                    */



}// end api caller
