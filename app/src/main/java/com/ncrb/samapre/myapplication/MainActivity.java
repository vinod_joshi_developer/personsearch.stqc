package com.ncrb.samapre.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    Button btn_login;
    Button btn_cancel;
    EditText edt_userid;
    String edt_userid1;
    EditText edt_password;
    String edt_password2;
    Singleton singleton;

    public ProgressDialog mProgressDialog;

    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        singleton = Singleton.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        setContentView(R.layout.activity_main);

        btn_login=(Button)findViewById(R.id.button_login);
        btn_cancel=(Button)findViewById(R.id.btn_cancel) ;
        edt_userid = (EditText) findViewById(R.id.edt_userid);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void login() {
        Utils.printv("hello hello");
        edt_userid = (EditText) findViewById(R.id.edt_userid);
        edt_userid1=edt_userid.getText().toString();
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_password2=edt_password.getText().toString();
        try {
            GetAuthDetailWebService(edt_userid1,edt_password2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void GetAuthDetailWebService(String username1, String password1) throws Exception {

        this.mProgressDialog.show();

        Utils.printv("post params hello vinod");


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("username", username1.toString());
            postParams.put("password", password1.toString());
            postParams.put("m_service", "mLoginVerify");

            // keep the username for next use
            this.singleton.username = username1.toString();

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            Utils.printv("post params without encode "+coco_seed);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }


        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);
        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mLoginVerify", postParams);



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        // -----------------------------------------------------------------

        apiCaller.mLoginVerify(jsonPostParams, new Callback<WSPLoginConnect>() {

            @Override
            public void failure(RetrofitError arg0) {

                Toast.makeText(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_LONG).show();


            }// end failure

            @Override
            public void success(WSPLoginConnect result, Response response) {

                System.out.println("RESULT status " + result.STATUS_CODE);

                if (result.STATUS_CODE.toString().equals("200")) {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();


                    //System.out.println("RESULT VJ getting offices");

                    Intent intent = new Intent(MainActivity.this, Home.class);
                    startActivity(intent);

                    //Toast.makeText(getApplicationContext(), "Bingo!!! ", Toast.LENGTH_SHORT);


                } else {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();


                    Toast.makeText(getApplicationContext(),"Try Again", Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), result.STATUS, Toast.LENGTH_SHORT);
                }
            }// end success
        });

    }
}// end auth web service

    /*private void onLoginSuccess() {
        btn_login.setEnabled(true);
        finish();
    }

    private void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        btn_login.setEnabled(true);
    }

    private boolean validate() {

        boolean valid = true;

        String userid = edt_userid.getText().toString();
        String password = edt_password.getText().toString();

        if (userid.isEmpty()) {
            edt_userid.setError("enter a valid useid");
            valid = false;
        } else {
            edt_userid.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            edt_password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            edt_password.setError(null);
        }

        return valid;
    }*/
