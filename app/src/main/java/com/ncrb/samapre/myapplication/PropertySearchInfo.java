package com.ncrb.samapre.myapplication;

public class PropertySearchInfo {

	public String FIR_NO;
	public String FIR_REGN_NUM;
	public String GD_No;
	public String Reg_Owner;
	public String MV_Type;
	public String REGISTRATION_NO;
	public String CHASSIS_NO;
	public String ENGINE_NO;
	public String Full_FIR_NUMBER;
	public String State;
	public String district;
	public String ps;
	public String propertyNature;
	public String mvModel;
	public String mvMake;
	public String mvColor;
	public String registrationNo;
	public String chasisNo;
	public String engineNo;
	public String STATE;
	public String mvType;
	public String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getChasisNo() {
		return chasisNo;
	}

	public void setChasisNo(String chasisNo) {
		this.chasisNo = chasisNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getSTATE() {
		return STATE;
	}

	public void setSTATE(String STATE) {
		this.STATE = STATE;
	}

	public String getMvType() {
		return mvType;
	}

	public void setMvType(String mvType) {
		this.mvType = mvType;
	}

	public PropertySearchInfo() {}

	public PropertySearchInfo(String FIR_NO, String GD_No
			, String reg_Owner, String MV_Type, String REGISTRATION_NO
			, String CHASSIS_NO, String ENGINE_NO, String full_FIR_NUMBER
			, String state, String district, String ps, String propertyNature
			, String mvModel, String mvMake, String mvColor) {

		this.FIR_NO = FIR_NO;
		this.GD_No = GD_No;
		this.Reg_Owner = reg_Owner;
		this.MV_Type = MV_Type;
		this.REGISTRATION_NO = REGISTRATION_NO;
		this.CHASSIS_NO = CHASSIS_NO;
		this.ENGINE_NO = ENGINE_NO;
		this.Full_FIR_NUMBER = full_FIR_NUMBER;
		this.State = state;
		this.district = district;
		this.ps = ps;
		this.propertyNature = propertyNature;
		this.mvModel = mvModel;
		this.mvMake = mvMake;
		this.mvColor = mvColor;
	}

    public String getFIR_REGN_NUM() {
        return FIR_REGN_NUM;
    }

    public void setFIR_REGN_NUM(String FIR_REGN_NUM) {
        this.FIR_REGN_NUM = FIR_REGN_NUM;
    }

    public String getFIR_NO() {
		return FIR_NO;
	}


	public void setFIR_NO(String FIR_NO) {
		this.FIR_NO = FIR_NO;
	}

	public String getGD_No() {
		return GD_No;
	}

	public void setGD_No(String GD_No) {
		this.GD_No = GD_No;
	}

	public String getReg_Owner() {
		return Reg_Owner;
	}

	public void setReg_Owner(String reg_Owner) {
		Reg_Owner = reg_Owner;
	}

	public String getMV_Type() {
		return MV_Type;
	}

	public void setMV_Type(String MV_Type) {
		this.MV_Type = MV_Type;
	}

	public String getREGISTRATION_NO() {
		return REGISTRATION_NO;
	}

	public void setREGISTRATION_NO(String REGISTRATION_NO) {
		this.REGISTRATION_NO = REGISTRATION_NO;
	}

	public String getCHASSIS_NO() {
		return CHASSIS_NO;
	}

	public void setCHASSIS_NO(String CHASSIS_NO) {
		this.CHASSIS_NO = CHASSIS_NO;
	}

	public String getENGINE_NO() {
		return ENGINE_NO;
	}

	public void setENGINE_NO(String ENGINE_NO) {
		this.ENGINE_NO = ENGINE_NO;
	}

	public String getFull_FIR_NUMBER() {
		return Full_FIR_NUMBER;
	}

	public void setFull_FIR_NUMBER(String full_FIR_NUMBER) {
		Full_FIR_NUMBER = full_FIR_NUMBER;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getPropertyNature() {
		return propertyNature;
	}

	public void setPropertyNature(String propertyNature) {
		this.propertyNature = propertyNature;
	}

	public String getMvModel() {
		return mvModel;
	}

	public void setMvModel(String mvModel) {
		this.mvModel = mvModel;
	}

	public String getMvMake() {
		return mvMake;
	}

	public void setMvMake(String mvMake) {
		this.mvMake = mvMake;
	}

	public String getMvColor() {
		return mvColor;
	}

	public void setMvColor(String mvColor) {
		this.mvColor = mvColor;
	}
}