package com.ncrb.samapre.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class PropertyDetails extends AppCompatActivity {
    ImageButton btn_Back;
    AppPreferences objAppPreferences;
    Singleton singleton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);
        TextView textView_property_fir_no;
        TextView textView_property_vehicle_type ;
        TextView textView_property_owner_name;
        TextView textView_REGISTRATION_NO;
        TextView textView_CHASSIS_NO;
        TextView textView_ENGINE_NO;
        TextView textView_Full_FIR_NUMBER;
        TextView textView_State;
        TextView textView_district;
        TextView textView_ps;
        TextView textView_propertyNature;
        TextView textView_model;
        TextView textView_color;
        TextView textView_manufacturer;
        TextView GD_No;
        Button btn_pdf;
        singleton = Singleton.getInstance();

        // core objects
        objAppPreferences = new AppPreferences(PropertyDetails.this);
        // update count value
        updateUserCount(PropertyDetails.this, Constants.SearchTypeProperty);
        btn_pdf=(Button)findViewById(R.id.btn_pdf);
        textView_property_fir_no=(TextView)findViewById(R.id.txt_property_fir_no);
        textView_property_vehicle_type=(TextView)findViewById(R.id.txt_vehicle_type);
        textView_property_owner_name=(TextView)findViewById(R.id.txt_property_owner_name);

        textView_REGISTRATION_NO=(TextView)findViewById(R.id.txt_REGISTRATION_NO);
        textView_CHASSIS_NO=(TextView)findViewById(R.id.txt_CHASSIS_NO);
        textView_ENGINE_NO=(TextView)findViewById(R.id.txt_ENGINE_NO);
        //textView_Full_FIR_NUMBER=(TextView)findViewById(R.id.txt_Full_FIR_NUMBER);
        textView_State=(TextView)findViewById(R.id.txt_State);
        textView_district=(TextView)findViewById(R.id.txt_district);
        textView_ps=(TextView)findViewById(R.id.txt_ps);
        textView_propertyNature=(TextView)findViewById(R.id.txt_propertyNature);
        textView_manufacturer=(TextView)findViewById(R.id.txt_vehicle_Manu);
        textView_model=(TextView)findViewById(R.id.txt_vehicle_Model);
        textView_color=(TextView)findViewById(R.id.txt_vehicle_color);
        GD_No=(TextView)findViewById(R.id.txt_property_GD_No);
        btn_Back=(ImageButton)findViewById(R.id.btn_Back);
        Bundle data= getIntent().getExtras();

        textView_property_fir_no.setText(data.getString("FIR_REGN_NUM"));
        textView_property_vehicle_type.setText(data.getString("mvType"));
        textView_property_owner_name.setText(data.getString("OWNER_NAME"));

        textView_REGISTRATION_NO.setText(data.getString("Registration_Number"));
        textView_CHASSIS_NO.setText(data.getString("CHASSIS_NO"));
        textView_ENGINE_NO.setText(data.getString("ENGINE_NO"));
        //textView_Full_FIR_NUMBER.setText(data.getString("Full_FIR_NUMBER"));
        textView_State.setText(data.getString("State"));
        textView_district.setText(data.getString("district"));
        textView_ps.setText(data.getString("PS"));
        textView_propertyNature.setText(data.getString("propertyNature"));
        textView_model.setText(data.getString("mvModel"));
        textView_manufacturer.setText(data.getString("mvMake"));
        textView_color.setText(data.getString("mvColor"));
        GD_No.setText(data.getString("GD_No"));



        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPDF(view);
            }
        });

    }
    public void printPDF(View view) {
        PrintManager printManager = (PrintManager) getSystemService(PRINT_SERVICE);
        printManager.print("print_any_view_job_name", new ViewPrintAdapter(this,findViewById(R.id.pdfLayout)), null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }

    public void updateUserCount(Context context, String searchType) {


        String date = Utils.getDateTime("dd-MM-yyyy");

        String uniqueKey = searchType+" "+date+" "+this.singleton.username;

        try {

            // core objects
            objAppPreferences = new AppPreferences(context);

            String checkUser = objAppPreferences.getUserDefaults(uniqueKey);

            if(!checkUser.equals("")) {
                int count = Integer.parseInt(checkUser);
                count++;
                objAppPreferences.setUserDefaults(uniqueKey, ""+count);
                Utils.printv("Set Preference "+uniqueKey+"count: "+count);
            } else {
                objAppPreferences.setUserDefaults(uniqueKey, "1");
                Utils.printv("Set Preference "+uniqueKey+"count: 1");
            }

        }catch (Exception e){
            e.printStackTrace();
            objAppPreferences.setUserDefaults(uniqueKey, "1");
            Utils.printv("Set Preference "+uniqueKey+"count: 1");
        }

    }// end update count
}
