package com.ncrb.samapre.myapplication;

public class PersonSearchInfo {


	public String FULL_NAME;
	public String GENDER;
	public String ALIAS1;
	public String RELIGION;
	public String RELATION_TYPE;
	public String RELATIVE_NAME;
	public String AGE;
	public String HEIGHT_FROM_CM;
	public String HEIGHT_TO_CM;
	public String STATE_ENG;
	public String DISTRICT;
	public String PS;
	public String NATIONALITY;
	public String MatchingCriteria;
	public String REGISTRATION_NUM;
	public String REG_DT;
	public String REG_NUM;
	public String UID_NUM;
	public String PERMANENT;
	public String PRESENT;
	public String UploadedFile;
	public String missRegNo;
	public String accusedsrNo;

	public String getAccusedsrNo() {
		return accusedsrNo;
	}

	public void setAccusedsrNo(String accusedsrNo) {
		this.accusedsrNo = accusedsrNo;
	}

	public String getMissRegNo() {
		return missRegNo;
	}

	public void setMissRegNo(String missRegNo) {
		this.missRegNo = missRegNo;
	}

	public PersonSearchInfo(String FULL_NAME, String GENDER, String ALIAS1, String RELIGION, String RELATION_TYPE, String RELATIVE_NAME, String AGE, String HEIGHT_FROM_CM, String HEIGHT_TO_CM, String STATE_ENG, String DISTRICT, String PS, String NATIONALITY, String matchingCriteria, String REGISTRATION_NUM, String REG_DT, String REG_NUM, String UID_NUM, String PERMANENT, String PRESENT, String uploadedFile) {
		this.FULL_NAME = FULL_NAME;
		this.GENDER = GENDER;
		this.ALIAS1 = ALIAS1;
		this.RELIGION = RELIGION;
		this.RELATION_TYPE = RELATION_TYPE;
		this.RELATIVE_NAME = RELATIVE_NAME;
		this.AGE = AGE;
		this.HEIGHT_FROM_CM = HEIGHT_FROM_CM;
		this.HEIGHT_TO_CM = HEIGHT_TO_CM;
		this.STATE_ENG = STATE_ENG;
		this.DISTRICT = DISTRICT;
		this.PS = PS;
		this.NATIONALITY = NATIONALITY;
		MatchingCriteria = matchingCriteria;
		this.REGISTRATION_NUM = REGISTRATION_NUM;
		this.REG_DT = REG_DT;
		this.REG_NUM = REG_NUM;
		this.UID_NUM = UID_NUM;
		this.PERMANENT = PERMANENT;
		this.PRESENT = PRESENT;
		UploadedFile = uploadedFile;
	}

	public String getFULL_NAME() {
		return FULL_NAME;
	}

	public void setFULL_NAME(String FULL_NAME) {
		this.FULL_NAME = FULL_NAME;
	}

	public String getGENDER() {
		return GENDER;
	}

	public void setGENDER(String GENDER) {
		this.GENDER = GENDER;
	}

	public String getALIAS1() {
		return ALIAS1;
	}

	public void setALIAS1(String ALIAS1) {
		this.ALIAS1 = ALIAS1;
	}

	public String getRELIGION() {
		return RELIGION;
	}

	public void setRELIGION(String RELIGION) {
		this.RELIGION = RELIGION;
	}

	public String getRELATION_TYPE() {
		return RELATION_TYPE;
	}

	public void setRELATION_TYPE(String RELATION_TYPE) {
		this.RELATION_TYPE = RELATION_TYPE;
	}

	public String getRELATIVE_NAME() {
		return RELATIVE_NAME;
	}

	public void setRELATIVE_NAME(String RELATIVE_NAME) {
		this.RELATIVE_NAME = RELATIVE_NAME;
	}

	public String getAGE() {
		return AGE;
	}

	public void setAGE(String AGE) {
		this.AGE = AGE;
	}

	public String getHEIGHT_FROM_CM() {
		return HEIGHT_FROM_CM;
	}

	public void setHEIGHT_FROM_CM(String HEIGHT_FROM_CM) {
		this.HEIGHT_FROM_CM = HEIGHT_FROM_CM;
	}

	public String getHEIGHT_TO_CM() {
		return HEIGHT_TO_CM;
	}

	public void setHEIGHT_TO_CM(String HEIGHT_TO_CM) {
		this.HEIGHT_TO_CM = HEIGHT_TO_CM;
	}

	public String getSTATE_ENG() {
		return STATE_ENG;
	}

	public void setSTATE_ENG(String STATE_ENG) {
		this.STATE_ENG = STATE_ENG;
	}

	public String getDISTRICT() {
		return DISTRICT;
	}

	public void setDISTRICT(String DISTRICT) {
		this.DISTRICT = DISTRICT;
	}

	public String getPS() {
		return PS;
	}

	public void setPS(String PS) {
		this.PS = PS;
	}

	public String getNATIONALITY() {
		return NATIONALITY;
	}

	public void setNATIONALITY(String NATIONALITY) {
		this.NATIONALITY = NATIONALITY;
	}

	public String getMatchingCriteria() {
		return MatchingCriteria;
	}

	public void setMatchingCriteria(String matchingCriteria) {
		MatchingCriteria = matchingCriteria;
	}

	public String getREGISTRATION_NUM() {
		return REGISTRATION_NUM;
	}

	public void setREGISTRATION_NUM(String REGISTRATION_NUM) {
		this.REGISTRATION_NUM = REGISTRATION_NUM;
	}

	public String getREG_DT() {
		return REG_DT;
	}

	public void setREG_DT(String REG_DT) {
		this.REG_DT = REG_DT;
	}

	public String getREG_NUM() {
		return REG_NUM;
	}

	public void setREG_NUM(String REG_NUM) {
		this.REG_NUM = REG_NUM;
	}

	public String getUID_NUM() {
		return UID_NUM;
	}

	public void setUID_NUM(String UID_NUM) {
		this.UID_NUM = UID_NUM;
	}

	public String getPERMANENT() {
		return PERMANENT;
	}

	public void setPERMANENT(String PERMANENT) {
		this.PERMANENT = PERMANENT;
	}

	public String getPRESENT() {
		return PRESENT;
	}

	public void setPRESENT(String PRESENT) {
		this.PRESENT = PRESENT;
	}

	public String getUploadedFile() {
		return UploadedFile;
	}

	public void setUploadedFile(String uploadedFile) {
		UploadedFile = uploadedFile;
	}
}